package core.tools;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Mitanjo
 */
public class FileFilterManager {
    private static FileFilterManager instance = new FileFilterManager();
    private String extension = "sor";

    private FileFilterManager() {}

    public static FileFilterManager getInstance() {
        return instance;
    }

    private FileNameExtensionFilter defaultFilter = new FileNameExtensionFilter("Asa soratra malagasy(*.sor)", extension);

    public void setFileChooserFilter(JFileChooser fileChooser) {
        if(fileChooser == null) return;

        fileChooser.setFileFilter(defaultFilter);
    }

    public String getExtension() {
        return "." + extension;
    }
}
