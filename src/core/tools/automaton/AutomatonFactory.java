package core.tools.automaton;

import core.tools.IAutomaton;
import neura.lang.malagasy.EnumMalagasyAutomaton;
import neura.lang.malagasy.MalagasyAutomatonFactory;
import neura.states.EnumState;
import neura.states.IState;
import neura.tools.StateRunner;

/**
 *
 * @author Mitanjo
 */
public class AutomatonFactory {
    private static AutomatonFactory instance = new AutomatonFactory();

    private AutomatonFactory() {
        initFactory();
    }

    public static AutomatonFactory getInstance() {
        return instance;
    }

    private IAutomaton q1, q4, q5, q6_8, q7_9_10_11_12, q13, q14, q15, q16, q17a, q17b, q17c, q17d, q18, q19, q21, q23, q24ext, q25ext;

    private void initFactory() {
        q1 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q1);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q4 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q4);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q5 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q5);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q6_8 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q6_8);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q7_9_10_11_12 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q7_9_10_11_12);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q13 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q13);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q14 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q14);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q15 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q15);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q16 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q16);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q17a = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q17A);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q17b = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q17B);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q17c = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q17C);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q17d = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q17D);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q18 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q18);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q19 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q19);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q21 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q21);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q23 = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q23);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q24ext = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q24EXT);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };

        q25ext = new IAutomaton() {
            public boolean test(String word) {
                IState iq = MalagasyAutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q25EXT);
                return (StateRunner.getInstance().runState(iq, word) == EnumState.FINAL_STATE);
            }
        };
    }

    public IAutomaton createAutomaton(EnumMalagasyAutomaton i) {
        switch(i) {
            case Q1 : return q1;
            case Q4 : return q4;
            case Q5 : return q5;
            case Q6_8 : return q6_8;
            case Q7_9_10_11_12: return q7_9_10_11_12;
            case Q13 : return q13;
            case Q14 : return q14;
            case Q15 : return q15;
            case Q16 : return q16;
            case Q17A : return q17a;
            case Q17B : return q17b;
            case Q17C : return q17c;
            case Q17D : return q17d;
            case Q18 : return q18;
            case Q19 : return q19;
            case Q21 : return q21;
            case Q23 : return q23;
            case Q24EXT : return q24ext;
            case Q25EXT : return q25ext;
        }

        return null;
    }
}
