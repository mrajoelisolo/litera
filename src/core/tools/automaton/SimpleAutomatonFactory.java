package core.tools.automaton;


import core.tools.IAutomaton;
import neura.lang.CharSet;
import neura.lang.malagasy.MalagasyAlphabet;

/**
 *
 * @author Mitanjo
 */
public class SimpleAutomatonFactory {
    private static SimpleAutomatonFactory instance = new SimpleAutomatonFactory();

    private SimpleAutomatonFactory() {
        initFactory();
    }

    public static SimpleAutomatonFactory getInstance() {
        return instance;
    }

    private IAutomaton q2, q20, q3, q26ext;

    private void initFactory() {
        q2 = new IAutomaton() {
            public boolean test(String word) {
                String str = word.toLowerCase().replace(",", "").replace(".", "");
                for(int i = 0; i < str.length() - 1; i++) {
                    String a = str.substring(i, i+1);
                    String b = str.substring(i+1, i+2);

                    if(MalagasyAlphabet.getInstance().getConsonants().contains(a) && MalagasyAlphabet.getInstance().getConsonants().contains(b))
                        if(a.equals(b) || !q2test(a+b)) return false;
                }

                return true;
            }

            public boolean q2test(String word) {
                    String[] dico = {"dr", "mb", "mp", "tr", "ts", "nk",  "nj", "nt", "nd"};

                    for(String s : dico) {
                        if(word.contains(s)) return true;
                    }

                    return false;
            }
        };

        q3 = new IAutomaton() {
            CharSet subVowel = new CharSet("eoy");

            public boolean test(String word) {
                String str = word.toLowerCase().replace(",", "").replace(".", "");
                for(int i = 0; i < str.length() - 1; i++) {
                    String a = str.substring(i, i+1);
                    String b = str.substring(i+1, i+2);

                    if(subVowel.contains(a) && subVowel.contains(b))
                        if(a.equals(b)) return false;
                }

                return true;
            }
        };

        q20 = new IAutomaton() {
            CharSet subVowel = new CharSet("eoy");
            
            public boolean test(String word) {
                String str = word.toLowerCase().replace(",", "").replace(".", "");
                for(int i = 0; i < str.length() - 1; i++) {
                    String a = str.substring(i, i+1);
                    String b = str.substring(i+1, i+2);

                    if(subVowel.contains(a) && subVowel.contains(b))
                        if(a.equals(b)) return false;
                }

                return true;
            }
        };

        q3 = new IAutomaton() {
            CharSet vowel = new CharSet("a");

            public boolean test(String word) {
                String str = word.toLowerCase().replace(",", "").replace(".", "");
                for(int i = 0; i < str.length() - 2; i++) {
                    String a = str.substring(i, i+1);
                    String b = str.substring(i+1, i+2);
                    String c = str.substring(i+2, i+3);

                    if(vowel.contains(a) && vowel.contains(b) && vowel.contains(c))
                        return false;
                }

                return true;
            }
        };
    }

    public IAutomaton createSimpleAutomaton(EnumSimpleAutomatonSet i) {
        switch(i) {
            case Q2: return q2;
            case Q3: return q3;
            case Q20: return q20;
        }

        return null;
    }
}
