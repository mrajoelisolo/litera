package core.tools;

/**
 *
 * @author Mitanjo
 */
public interface IAutomaton {
    public boolean test(String word);
}
