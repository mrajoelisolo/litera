package core.tools;

/**
 *
 * @author Mitanjo
 */
public class HolyWords {
    private static HolyWords instance = new HolyWords();

    private HolyWords() {}

    public static HolyWords getInstance() {
        return instance;
    }

    public boolean verify(String str) {
        String words[] = {"Jehovah", "Iaveh", "Jesosy", "Kristy", "Lakroa"};

        for(String s : words) {
            if(str.equals(s)) return true;
        }

        return false;
    }
}
