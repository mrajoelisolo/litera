package core.tools;

import java.awt.Image;
import javax.swing.JDialog;
import javax.swing.JFrame;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class IconManager {
    private static IconManager instance = new IconManager();

    private IconManager() {}

    public static IconManager getInstance() {
        return instance;
    }

    private Image icon = LWResourceLoader.getInstance().loadImage("img/icon.png");

    public void initialize(JFrame frame) {
        if(frame == null) return;

        frame.setIconImage(icon);
    }

    public void initialize(JDialog frame) {
        if(frame == null) return;

        frame.setIconImage(icon);
    }
}
