package core.tools;

import javax.swing.JFrame;

/**
 *
 * @author Mitanjo
 */
public class FrameUtilities {
    private static FrameUtilities instance = new FrameUtilities();

    private FrameUtilities() {}

    public static FrameUtilities getInstance() {
        return instance;
    }

    public void show(JFrame frame) {
        if(frame == null) return;

        frame.setVisible(true);
    }
}
