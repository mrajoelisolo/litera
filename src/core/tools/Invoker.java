package core.tools;

/**
 *
 * @author Mitanjo
 */
public class Invoker {
    private static Invoker instance = new Invoker();

    private Invoker() {}

    public static Invoker getInstance() {
        return instance;
    }

    public void invoke(ICommand cmd) {
        if(cmd == null) return;

        cmd.doing();
    }
}
