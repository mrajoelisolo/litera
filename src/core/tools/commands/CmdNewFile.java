package core.tools.commands;

import core.MainFrame;
import core.editor.EditorTool;
import core.tools.*;
import javax.swing.*;

/**
 *
 * @author Mitanjo
 */
public class CmdNewFile implements ICommand {    
    private JFrame frame;
    private EditorTool editor;

    public CmdNewFile(JFrame frame, EditorTool editor) {        
        this.frame = frame;
        this.editor = editor;
    }

    public void doing() {
        if(frame == null) return;
        if(editor == null) return;

        if(editor.isDirty()) {
            int res = JOptionPane.showConfirmDialog(frame, "Tehirizina ve ny asa taloha", "Hamorina asa vaovao", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

            if(res == JOptionPane.CANCEL_OPTION) return;
            if(res == JOptionPane.YES_OPTION) editor.saveDocument(frame);
        }
        editor.clearDocument();

        String title = MainFrame.FRAME_TITLE + " - [Asa vaovao]";
        frame.setTitle(title);
    }
}
