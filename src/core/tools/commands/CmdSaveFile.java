package core.tools.commands;

import core.MainFrame;
import core.editor.EditorTool;
import core.tools.*;
import java.io.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.text.*;

/**
 *
 * @author Mitanjo
 */
public class CmdSaveFile implements ICommand {
    private static JFileChooser fileChooser;
    private JFrame frame;
    private EditorTool editor;

    public CmdSaveFile(JFrame frame, EditorTool editor) {
        if(fileChooser == null) {
            fileChooser = new JFileChooser();
            fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
            FileFilterManager.getInstance().setFileChooserFilter(fileChooser);
        }

        this.frame = frame;
        this.editor = editor;
    }

    public void doing() {
        if(frame == null) return;

        if(!editor.isDirty()) return;
        
        if(!editor.fileExists()) {
            int res = fileChooser.showDialog(frame, null);
            if(res == JFileChooser.APPROVE_OPTION) {
                try {
                    File selectedFile = fileChooser.getSelectedFile();
                    String filePath = selectedFile.getPath() + FileFilterManager.getInstance().getExtension();
                    FileOutputStream fos = new FileOutputStream(filePath);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    Document doc = editor.getDocument();
                    oos.writeObject(doc);

                    oos.flush();
                    fos.close();

                    editor.setDirty(false);
                    editor.setDocPath(filePath);

                    String title = MainFrame.FRAME_TITLE + " - [" + selectedFile.getName() + "]";
                    frame.setTitle(title);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(CmdSaveFile.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(CmdSaveFile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }else {
            try{
                FileOutputStream fos = new FileOutputStream(editor.getDocPath());

                ObjectOutputStream oos = new ObjectOutputStream(fos);
                Document doc = editor.getDocument();
                oos.writeObject(doc);

                oos.flush();
                fos.close();

                editor.setDirty(false);

//                String title = frame.getTitle() + " - [" + fos.getFD() + "]";
//                frame.setTitle(title);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(EditorTool.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(EditorTool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
