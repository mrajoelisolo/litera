package core.tools.commands;

import core.editor.EditorTool;
import core.tools.ICommand;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Mitanjo
 */
public class CmdCloseApplication implements ICommand {
    private JFrame frame;
    private EditorTool editor;

    public CmdCloseApplication(JFrame frame, EditorTool editor) {
        this.frame = frame;
        this.editor = editor;
    }

    public void doing() {
        if(frame == null) return;
        if(editor == null) return;

        if(frame == null) return;
        if(editor == null) return;

        if(editor.isDirty()) {
            int res = JOptionPane.showConfirmDialog(frame, "Tehirizina ve ny asa ?", "Hanakatona asa", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

            if(res == JOptionPane.CANCEL_OPTION) return;
            if(res == JOptionPane.YES_OPTION) editor.saveDocument(frame);
        }

        System.exit(1);
    }
}
