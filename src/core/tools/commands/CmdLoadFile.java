package core.tools.commands;

import core.MainFrame;
import core.editor.EditorTool;
import core.tools.*;
import java.io.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.text.*;

/**
 *
 * @author Mitanjo
 */
public class CmdLoadFile implements ICommand {
    private static JFileChooser fileChooser;
    private JFrame frame;
    private File selectedFile;
    private EditorTool editor;
    
    public CmdLoadFile(JFrame frame, EditorTool editor) {
        if(fileChooser == null) {
            fileChooser = new JFileChooser();
            fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
            FileFilterManager.getInstance().setFileChooserFilter(fileChooser);
        }

        this.frame = frame;
        this.editor = editor;
    }

    public void doing() {
        if(frame == null) return;
        if(editor == null) return;

        //Confirm if last modifications will be saved
        if(editor.isDirty()) {
            int res = JOptionPane.showConfirmDialog(frame, "Tehirizina ve ny asa taloha", "Hanokatra asa vaovao", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

            if(res == JOptionPane.CANCEL_OPTION) return;
            if(res == JOptionPane.YES_OPTION) editor.saveDocument(frame);
        }

        int res = fileChooser.showDialog(frame, null);        
        if(res == JFileChooser.APPROVE_OPTION) {
            try {
                selectedFile = fileChooser.getSelectedFile();
                FileInputStream fis = new FileInputStream(selectedFile);
                ObjectInputStream ois = new ObjectInputStream(fis);
                Document doc = (Document) ois.readObject();

                editor.setDocument((DefaultStyledDocument) doc);

                fis.close();
                ois.close();

                editor.setDirty(false);
                editor.setDocPath(selectedFile.getPath());

                String title = MainFrame.FRAME_TITLE + " - [" + selectedFile.getName() + "]";
                frame.setTitle(title);
            } catch (ClassNotFoundException ex) {
                JOptionPane.showMessageDialog(frame, "Internal error: " + ex, "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(CmdLoadFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(frame, "Internal error: " + ex, "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(CmdLoadFile.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(frame, "Internal error: " + ex, "Error", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(CmdLoadFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    }
}
