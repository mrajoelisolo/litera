package core.tools;

/**
 *
 * @author Mitanjo
 */
public class MalagasyWords {
    private static MalagasyWords instance = new MalagasyWords();

    private MalagasyWords() {}

    public static MalagasyWords getInstance() {
        return instance;
    }

    public boolean equals(String str) {
        String words[] = {"amin'ny", "tamin'ny", "tamin'i"};

        for(String s : words) {
            if(str.toLowerCase().equals(s)) return true;
        }

        return false;
    }
}
