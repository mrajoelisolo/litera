package core.tools;

import core.tools.automaton.EnumSimpleAutomatonSet;
import core.tools.automaton.AutomatonFactory;
import core.tools.automaton.SimpleAutomatonFactory;
import neura.lang.malagasy.EnumMalagasyAutomaton;

/**
 *
 * @author Mitanjo
 */
public class AutomatonSet {
    private static AutomatonSet instance = new AutomatonSet();

    private AutomatonSet() {
        initialize();
    }

    public static AutomatonSet getInstance() {
        return instance;
    }
    
    private IAutomaton q1, q2, q3, q4, q5, q6_8, q7_9_10_11_12, q13, q14, q15, q16, q17a, q17b, q17c, q17d, q18, q19, q20, q21, q23, q24ext, q25ext;
    private boolean b1, b2, b3, b4, b5, b6_8, b7_9_10_11_12, b13, b14, b15, b16, b17a, b17b, b17c, b17d, b18, b19, b20, b21, b23, b24ext, b25ext;

    private void initialize() {        
        q1 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q1);
        q2 = SimpleAutomatonFactory.getInstance().createSimpleAutomaton(EnumSimpleAutomatonSet.Q2);
        q3 = SimpleAutomatonFactory.getInstance().createSimpleAutomaton(EnumSimpleAutomatonSet.Q3);
        q4 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q4);
        q5 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q5);
        q6_8 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q6_8);
        q7_9_10_11_12 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q7_9_10_11_12);
        q13 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q13);
        q14 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q14);
        q15 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q15);
        q16 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q16);
        q17a = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q17A);
        q17b = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q17B);
        q17c = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q17C);
        q17d = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q17D);
        q18 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q18);
        q19 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q19);
        q20 = SimpleAutomatonFactory.getInstance().createSimpleAutomaton(EnumSimpleAutomatonSet.Q20);
        q21 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q21);
        q23 = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q23);
        q24ext = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q24EXT);
        q25ext = AutomatonFactory.getInstance().createAutomaton(EnumMalagasyAutomaton.Q25EXT);     
    }

    public boolean analyze(String word) {
        b1 = q1.test(word); //Un mot malagasy ne se termine jamais par la lettre 'i'
        b2 = q2.test(word); //Une consonne ne se répète jamais plus d'une fois
        b3 = q3.test(word);
        b4 = q4.test(word); //Un mot malagasy s'il contient la lettre y doit se trouver a la fin
        b5 = q5.test(word); //Un mot malagasy doit toujours se terminer par a,e,o ou y (absorbe 1,4 et 21)
        b6_8 = q6_8.test(word); //
        b7_9_10_11_12 = q7_9_10_11_12.test(word);
        b13 = q13.test(word);
        b14 = q14.test(word);
        b15 = q15.test(word);
        b16 = q16.test(word);
        b17a = q17a.test(word);
        b17b = q17b.test(word);
        b17c = q17c.test(word);
        b17d = q17d.test(word);
        b18 = q18.test(word);
        b19 = q19.test(word); //La lettre i est un mot malagasy
        b20 = q20.test(word); //Les voyelles e,o et y ne se répètent jamais au nombre de 2
        b21 = q21.test(word); //Un mot malagasy ne se termine par aucune consonne
        b23 = q23.test(word); //Un mot malagasy ne contient pas les lettres c, q, u, w, x
        b24ext = q24ext.test(word);
        b25ext = q25ext.test(word);

        //return ((b1 && b21 && b2 && b4 && b23) || b19) ; // tsy mande le b4
        //return ((b1 && b21 && b2 && b23) || b19) ;
        //return ((b5 && b2 && b20 && b23) || b19) ; //11_08_2010
        //return ((b5 && b2 && b6_8 && b7_9_10_11_12 && b20 && b23) || b19) ; //12_08_2010
        //return ((b5 && b2 && b3 && b6_8 && b7_9_10_11_12 && b13 && b14 && b18 && b20 && b23) || b19); //13_08_2010
        //return ((b5 && b2 && b3 && b6_8 && b7_9_10_11_12 && b13 && b14 && b15 && b18 && b20 && b23) || b19); //13_08_2010
        //return ((b5 && b2 && b3 && b6_8 && b7_9_10_11_12 && b13 && b14 && b15 && b16 && b18 && b20 && b23) || b19); //13_08_2010
        return ((b5 && b2 && b3 && b6_8 && b7_9_10_11_12 && b13 && b14 && b15 && b16 && b17a && b17b && b17c && b17d && b18 && b20 && b23
                && b24ext && b25ext) || b19 || MalagasyWords.getInstance().equals(word)
                || HolyWords.getInstance().verify(word)); //13_08_2010
    }
}
