package core.checker;

/**
 *
 * @author Mitanjo
 */
public interface ICheckerObserver {
    public void notify(String message);
}
