package core.checker;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Mitanjo
 */
public class Levenshtein {
    private static Levenshtein instance = new Levenshtein();

    private Levenshtein() {}

    public static Levenshtein getInstance() {
        return instance;
    }

    private String path = "data/words.txt";

    public int getDistance(String a, String b) {
        int n = a.length();
        int m = b.length();
        int cost;
        int[][] d = new int[n+1][m+1];

        for(int i = 0; i <= n; i++) {
            d[i][0] = i;
        }

        for(int j = 0; j <= m; j++) {
            d[0][j] = j;
        }        

        for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= m; j++) {
                if(a.charAt(i-1) == b.charAt(j-1)) cost = 0;
                else cost = 1;

                d[i][j] = getMin(d[i-1][j]+1, d[i][j-1]+1, d[i-1][j-1]+cost);
            }
        }

        return d[n][m];
    }

    public int getMin(int a, int b, int c) {
        if(a < b && a < c) return a;
        else if(b < a && b < c) return b;
        return c;
    }

    public List<String> getSuggestions(String str, int maxDistance, int maxDelta) {
        List<String> res = new ArrayList();

        try {
            BufferedReader file = new BufferedReader(new FileReader(path));
            String line;

            while((line = file.readLine()) != null) {
                if(str.length() - line.length() <= maxDelta)
                    if(getDistance(str, line) <= maxDistance)
                        res.add(line);
            }
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return res;
    }

    public List<String> getSuggestions(String str) {
        return getSuggestions(str, 2, 4);
    }
}