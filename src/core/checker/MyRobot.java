package core.checker;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mitanjo
 */
public class MyRobot {
    private static MyRobot instance = new MyRobot();
    private Robot robot;

    private MyRobot() {
        try {
            robot = new Robot();
            
        } catch (AWTException ex) {
            Logger.getLogger(MyRobot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static MyRobot getInstance() {
        return instance;
    }

    public void doing() {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                robot.mousePress(InputEvent.BUTTON1_MASK);
                robot.mouseRelease(InputEvent.BUTTON1_MASK);

                robot.mousePress(InputEvent.BUTTON1_MASK);
                robot.mouseRelease(InputEvent.BUTTON1_MASK);
            }
        });
        thread.start();
    }

    public void sendKey(int keyCode) {
        robot.keyPress(keyCode);
        robot.keyRelease(keyCode);
    }
}
