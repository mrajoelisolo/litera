package core.checker;

import core.tools.AutomatonSet;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.*;

/**
 *
 * @author Mitanjo
 */
public class WordChecker implements ActionListener {
    private static WordChecker instance = new WordChecker();

    private JPopupMenu popupMenu = new JPopupMenu();
    private ICheckerObserver currentObs = null;
    private static final int MAX_SUGGESTIONS = 15;

    private WordChecker() {}

    public static WordChecker getInstance() {
        return instance;
    }    

    public void showSuggestions(String str, MouseEvent ev, ICheckerObserver observer) {
        if(popupMenu == null && observer == null && str == null) return;

        boolean bCorrect = AutomatonSet.getInstance().analyze(str);
        if(bCorrect) return;

        this.currentObs = observer;
        popupMenu.removeAll();

        List<String> suggestions = Levenshtein.getInstance().getSuggestions(str, 1, 2);

        if(suggestions.size() == 0) {
            JMenuItem item = new JMenuItem("Tsy misy fanitsiana");
            item.setEnabled(false);
            popupMenu.add(item);
            popupMenu.show(ev.getComponent(), ev.getX(), ev.getY());

            return;
        }

        int i = 0;
        for(String suggestion : suggestions) {
            JMenuItem item = new JMenuItem(suggestion);
            item.addActionListener(this);
            popupMenu.add(item);
            
            if(i++ > MAX_SUGGESTIONS) break;
        }                

        popupMenu.show(ev.getComponent(), ev.getX(), ev.getY());
    }

    public void actionPerformed(ActionEvent e) {
        if(currentObs == null) return;

        Object o = e.getSource();

        if(o instanceof JMenuItem) {
            JMenuItem item = (JMenuItem) o;

            currentObs.notify(item.getText());
        }
    }
}
