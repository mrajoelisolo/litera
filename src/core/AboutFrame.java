/*
 * AboutFrame.java
 *
 * Created on 15 août 2010, 21:03:53
 */

package core;

import core.shapes.EnumProductBox;
import core.shapes.ProductBox;
import core.shapes.ProductBoxFactory;
import core.tools.IconManager;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JFrame;
import lwcanvas.LWCanvas;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class AboutFrame extends javax.swing.JDialog implements Runnable {
    private Thread myThread;
    private LWCanvas canvas;
    private Image imgProduct;
    private ProductBox box1;
    private ProductBox box2;

    /** Creates new form AboutFrame */
    public AboutFrame(JFrame parent) {
        super(parent, true);
        initComponents();
        initFrame(parent);
        initThread();
    }

    private void initThread() {
        myThread = new Thread(this);
        myThread.start();
    }

    private void initFrame(JFrame parent) {
        IconManager.getInstance().initialize(this);
        setLocationRelativeTo(parent);

        box1 = ProductBoxFactory.getInstance().createProductBox(EnumProductBox.BOX1);
        box2 = ProductBoxFactory.getInstance().createProductBox(EnumProductBox.BOX2);
        box1.setLocation(300, 10);
        box2.setLocation(475, 10);

        box1.setElevation(200);
        box2.setElevation(250);

        imgProduct = LWResourceLoader.getInstance().loadImage("img/about/project_logo.jpg");
        canvas = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
                if(imgProduct != null)
                    g.drawImage(imgProduct, 0, 0, 640, 300, null);
            }
        };
        canvas.setLayout(null);
        canvas.setBounds(0, 0, panelLogo.getBounds().width, panelLogo.getBounds().height);
        panelLogo.add(canvas);        

        canvas.addShape(box1);
        canvas.addShape(box2);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelLogo = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtProduct = new javax.swing.JTextArea();
        btnClose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("About Litera project");
        setMinimumSize(new java.awt.Dimension(645, 507));
        setResizable(false);
        getContentPane().setLayout(null);

        panelLogo.setBackground(new java.awt.Color(0, 0, 0));
        panelLogo.setMinimumSize(new java.awt.Dimension(640, 300));

        javax.swing.GroupLayout panelLogoLayout = new javax.swing.GroupLayout(panelLogo);
        panelLogo.setLayout(panelLogoLayout);
        panelLogoLayout.setHorizontalGroup(
            panelLogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 640, Short.MAX_VALUE)
        );
        panelLogoLayout.setVerticalGroup(
            panelLogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        getContentPane().add(panelLogo);
        panelLogo.setBounds(0, 0, 640, 300);

        txtProduct.setColumns(20);
        txtProduct.setEditable(false);
        txtProduct.setRows(5);
        txtProduct.setText("Product: Litera version 1.2\nAutomaton Tools framework: Neura 1.0\nDesign frameworks: LWFramework 1.2\nComponents: Swing set components\nJava look and feel thème: Nimbus\n\nNeura and the LWFramework are trademarks and/or registered trademarks of Myrmidon Software Engineering Team Inc. \nAll rights reserved.\n\nInformatique de Gestion, Génie Logiciel et Intelligence Artificielle\n3éme année 2009 - 2010, Institut Supérieur Polytechnique de Madagascar\n\nDevelopment team:\n- RAJOELISOLO Mitanjo n°2\n- RATSIMISETA Rina n°4\n- ANDRIAMAHASOLO Ader n°22\n- RAKOTOSON Fifaliana n°14\n- ANDRIANAVALONOME Njaka n°26\n- RAHARINANDRASANA Ony n°30");
        jScrollPane1.setViewportView(txtProduct);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(10, 310, 620, 120);

        btnClose.setText("Close");
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        getContentPane().add(btnClose);
        btnClose.setBounds(10, 440, 100, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        dispose();
        System.gc();
    }//GEN-LAST:event_btnCloseActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelLogo;
    private javax.swing.JTextArea txtProduct;
    // End of variables declaration//GEN-END:variables

    private void core() {
        int elevation = box1.getElevation();
        if(elevation > 0) {
            elevation = elevation - 7;
            box1.setElevation(elevation);
        }else
            box1.setElevation(0);

        elevation = box2.getElevation();
        if(elevation > 0) {
            elevation = elevation - 7;
            box2.setElevation(elevation);
        }else
            box2.setElevation(0);

        canvas.repaint();
    }

    public void run() {
        while(true) {
            try {
                if(Thread.currentThread().equals(myThread)) {
                    Thread.sleep(30);
                    core();
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
}
