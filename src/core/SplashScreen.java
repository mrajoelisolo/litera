package core;

import core.tools.FrameUtilities;
import core.tools.IconManager;
import java.awt.Graphics;
import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import lwcanvas.LWCanvas;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class SplashScreen extends JFrame implements Runnable {
    private Thread myThread;
    private int count = 0;

    private LWCanvas canvas;
    private Image bcgImg = LWResourceLoader.getInstance().loadImage("img/splash/splash.jpg");

    public SplashScreen() {
        initComponents();

        myThread = new Thread(this);
        myThread.start();
    }

    private void initComponents() {
        IconManager.getInstance().initialize(this);
        setUndecorated(true);
        setSize(800, 380);
        setLocationRelativeTo(this);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        canvas = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
               if(bcgImg != null)
                   g.drawImage(bcgImg, 0, 0, getBounds().width, getBounds().height, null);
            }
        };
        canvas.setLocation(0, 0);
        canvas.setSize(getBounds().width, getBounds().height);
        getContentPane().add(canvas);
    }

    private void core() {
        count++;
    }

    public void run() {
        while(count < 3) {
            try {
                core();
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(SplashScreen.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        dispose();
        FrameUtilities.getInstance().show(new MainFrame());
    }
}
