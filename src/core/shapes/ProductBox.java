package core.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractInteractiveShape;

/**
 *
 * @author Mitanjo
 */
public class ProductBox  extends LWAbstractInteractiveShape implements IShape {
    private ImageFrame imgBox;
    private ImageFrame imgMirror;
    private int elevation = 0;
    private Point location = new Point();

    public ProductBox(ImageFrame imgBox, ImageFrame imgMirror) {
        this.imgBox = imgBox;
        this.imgMirror = imgMirror;

        imgBox.setParent(this);
        imgMirror.setParent(this);
    }

    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        if(super.isFocused()) {
            imgBox.setAlphaValue(1f);
            imgMirror.setAlphaValue(0.4f);
        }else {
            imgBox.setAlphaValue(0.75f);
            imgMirror.setAlphaValue(0.2f);
        }

       imgMirror.draw(g2);
       imgBox.draw(g2);       
    }

    public boolean contains(Point p) {
        return imgBox.contains(p);
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(int x, int y) {
        this.location.setLocation(x, y);
        actualize();
    }

    public void actualize() {
        imgBox.actualize();
        imgMirror.actualize();
    }

    public void setElevation(int elevation) {
        this.elevation = elevation;
        imgBox.setElevation(-elevation);
        imgMirror.setElevation(elevation);
        actualize();
    }

    public int getElevation() {
        return elevation;
    }
}
