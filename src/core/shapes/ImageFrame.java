package core.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import lwcanvas.shapes.CanvasSettings;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class ImageFrame extends LWAbstractShape implements IShape {
    private Rectangle bounds = new Rectangle();
    private Rectangle pseudoBounds = new Rectangle();
    private Image img;
    private ProductBox parent;
    private int elevation = 0;

    public ImageFrame(Image img, int x, int y, int wdt, int hgt) {
        this.img = img;
        this.setBounds(x, y, wdt, hgt);
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public void draw(Graphics g) {
       Graphics2D g2 = (Graphics2D) g;

       CanvasSettings.getInstance().saveGraphics(g2);
       LWImgUtil.getInstance().setComposite(g2, alphaValue);

       g2.drawImage(img, pseudoBounds.x, pseudoBounds.y, pseudoBounds.width, pseudoBounds.height, null);

       CanvasSettings.getInstance().restoreGraphics(g2);
    }

    public boolean contains(Point p) {
        return pseudoBounds.contains(p);
    }

    public void setParent(ProductBox parent) {
        this.parent = parent;
    }

    public void actualize() {
        pseudoBounds.setBounds(bounds.x + parent.getLocation().x, bounds.y + parent.getLocation().y + elevation, bounds.width, bounds.height);
    }

    public int getElevation() {
        return elevation;
    }

    public void setElevation(int elevation) {
        this.elevation = elevation;
    }
}
