package core.shapes;

import java.awt.Image;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class ProductBoxFactory {
    private static ProductBoxFactory instance = new ProductBoxFactory();

    private ProductBoxFactory() {}

    public static ProductBoxFactory getInstance() {
        return instance;
    }

    public ProductBox createProductBox(EnumProductBox box) {
        ProductBox res = null;

        switch(box) {
            case BOX1: {
                Image img = LWResourceLoader.getInstance().loadImage("img/box/box1.png");
                Image mirr = LWResourceLoader.getInstance().loadImage("img/box/box1r.png");

                ImageFrame imgF = new ImageFrame(img, 0, 0, 120, 175);
                ImageFrame mirrF = new ImageFrame(mirr, 0, 160, 120, 175);
                res = new ProductBox(imgF, mirrF);

            }break;

            case BOX2: {
                Image img = LWResourceLoader.getInstance().loadImage("img/box/box2.png");
                Image mirr = LWResourceLoader.getInstance().loadImage("img/box/box2r.png");

                ImageFrame imgF = new ImageFrame(img, 0, 0, 120, 175);
                ImageFrame mirrF = new ImageFrame(mirr, 0, 160, 120, 175);
                res = new ProductBox(imgF, mirrF);
            }break;
        }

        return res;
    }
}
