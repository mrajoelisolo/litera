package core;

import java.awt.EventQueue;
import lwcanvas.util.LWMakeNimbusLookNFeel;

/**
 *
 * @author Mitanjo
 */
public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                LWMakeNimbusLookNFeel.getInstance().makeIt();
                new SplashScreen().setVisible(true);
            }
        });
    }
}
