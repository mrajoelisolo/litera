package core;

import core.checker.*;
import java.util.logging.*;
import javax.swing.*;
import core.editor.*;
import core.tools.*;
import core.tools.commands.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import lwcanvas.LWCanvas;
import lwcanvas.components.LWButton2;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class MainFrame extends JFrame implements Runnable, ICheckerObserver {
    public static final String FRAME_TITLE = "Naotipady vita malagasy";
    private Thread thread;
    private LWCanvas toolBar;
    private LWCanvas statusBar;
    private String statusText;
    private JScrollPane scrollPane;
    private EditorTool editorTool;
    private JEditorPane editor;
    private boolean bCheckSpell = false;

    private LWButton2 btnMenu1;
    private LWButton2 btnMenu2;
    private LWButton2 btnMenu3;
    private LWButton2 btnMenu4;
    private LWButton2 btnNeura;
    private Image imgMenuToolbar;
    private Image bcgToolbarMenu1;
    private Image bcgToolbarMenu2;
    private Image imgStatusBar;

    public MainFrame() {
        initComponents();
        initThread();
    }

    private void initThread() {
        thread = new Thread(this);
        thread.start();
    }

    private void initComponents() {
        IconManager.getInstance().initialize(this);
        setTitle(FRAME_TITLE + " - [Asa vaovao]");
        setSize(800, 627);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        getContentPane().setLayout(null);
        getContentPane().setBackground(Color.BLACK);

        WindowAdapter wa = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                formWindowClosing(evt);
            }
        };
        this.addWindowListener(wa);

        bcgToolbarMenu1 = LWResourceLoader.getInstance().loadImage("img/toolbar/toolbar_bcg1.png");
        bcgToolbarMenu2 = LWResourceLoader.getInstance().loadImage("img/toolbar/toolbar_bcg2.png");
        imgMenuToolbar = LWResourceLoader.getInstance().loadImage("img/toolbar/horToolbar.png");
        imgStatusBar = LWResourceLoader.getInstance().loadImage("img/toolbar/statusbar.png");

        toolBar = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
                if(imgMenuToolbar != null)
                    g.drawImage(imgMenuToolbar, 5, 5, 607, 19, null);

                if(bcgToolbarMenu1 != null)
                    g.drawImage(bcgToolbarMenu1, 5, 25, 220, 65, null);

                if(bcgToolbarMenu2 != null)
                    g.drawImage(bcgToolbarMenu2, 228, 25, 384, 65, null);
            }
        };
        toolBar.setBounds(5, 5, 785, 95);        
        toolBar.setLayout(null);
        getContentPane().add(toolBar);

        statusBar = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {     
                if(imgStatusBar != null)
                    g.drawImage(imgStatusBar, 0, 0, 790, 29, null);

                g.setColor(Color.BLACK);
                g.setFont(FontProvider.getInstance().getFont(EnumFont.STATUSBAR_FONT));
                g.drawString(statusText, 5, 15);
            }
        };
        statusBar.setBounds(5, 563, 785, 23);
        statusBar.setLayout(null);
        getContentPane().add(statusBar);

        editorTool = EditorProvider.getInstance().createEditorSet();
        editor = editorTool.getEditorPane();
        editor.setText("");

        editor.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        scrollPane = new JScrollPane();
        scrollPane.setBounds(5, 105, 785, 450);
        scrollPane.setViewportView(editor);
        getContentPane().add(scrollPane);

        float toolBarAlpha = 0.7f;

        btnNeura = new LWButton2("Neura") {
            @Override
            public void actionPerformed() {
                btnNeuraActionPerformed();
            }
        };
        btnNeura.setBounds(613, 5, 170, 85);
        btnNeura.setTextVisible(false);
        btnNeura.setAlphaValue(toolBarAlpha);
        btnNeura.setBtnSkin(BtnSkinProvider.getInstance().getBtnSkin(EnumBtnSkin.BTN_NEURA));
        toolBar.addShape(btnNeura);
        
        btnMenu1 = new LWButton2("New") {
            @Override
            public void actionPerformed() {
                btn1ActionPerformed();
            }
        };
        btnMenu1.setBounds(8, 28, 53, 59);
        btnMenu1.setAlphaValue(toolBarAlpha);
        btnMenu1.setTextVisible(false);
        btnMenu1.setBtnSkin(BtnSkinProvider.getInstance().getBtnSkin(EnumBtnSkin.BTN_NEW));
        toolBar.addShape(btnMenu1);

        btnMenu2 = new LWButton2("Open") {
            @Override
            public void actionPerformed() {
                btn2ActionPerformed();
            }
        };
        btnMenu2.setBounds(61, 28, 53, 59);
        btnMenu2.setAlphaValue(toolBarAlpha);
        btnMenu2.setTextVisible(false);
        btnMenu2.setBtnSkin(BtnSkinProvider.getInstance().getBtnSkin(EnumBtnSkin.BTN_OPEN));
        toolBar.addShape(btnMenu2);

        btnMenu3 = new LWButton2("Save") {
            @Override
            public void actionPerformed() {
                btn3ActionPerformed();
            }
        };
        btnMenu3.setBounds(114, 28, 53, 59);
        btnMenu3.setAlphaValue(toolBarAlpha);
        btnMenu3.setTextVisible(false);
        btnMenu3.setBtnSkin(BtnSkinProvider.getInstance().getBtnSkin(EnumBtnSkin.BTN_SAVE));
        toolBar.addShape(btnMenu3);

        btnMenu4 = new LWButton2("Check") {
            @Override
            public void actionPerformed() {
                btn4ActionPerformed();
            }
        };
        btnMenu4.setBounds(167, 28, 53, 59);
        btnMenu4.setAlphaValue(toolBarAlpha);
        btnMenu4.setTextVisible(false);
        btnMenu4.setBtnSkin(BtnSkinProvider.getInstance().getBtnSkin(EnumBtnSkin.BTN_UNCHECK));
        toolBar.addShape(btnMenu4);

        editor.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    rightMouseActionPerformed(e);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    mouseReleasedActionPerformed(e);
                }
            }
        });
    }

    //Since release2
    private void rightMouseActionPerformed(MouseEvent e) {
        MyRobot.getInstance().doing();
    }

    public void mouseReleasedActionPerformed(MouseEvent e) {
        String str = editor.getSelectedText();

        if(str == null) return;

        WordChecker.getInstance().showSuggestions(str, e, this);
    }
    //

    private void checkSpell() {
        if(bCheckSpell)
            //TextAnalyzer.getInstance().analyze(editorTool);
            TextAnalyzer.getInstance().analyze(editorTool);
    }

    private void resetCheckSpell() {
        editorTool.resetDocument();
    }

    private void formWindowClosing(WindowEvent evt) {
        CmdCloseApplication cmd = new CmdCloseApplication(this, editorTool);
        Invoker.getInstance().invoke(cmd);
    }

    private void btnNeuraActionPerformed() {
        AboutFrame f = new AboutFrame(this);
        f.setVisible(true);
    }

    private void btn1ActionPerformed() {
        CmdNewFile cmd = new CmdNewFile(this, editorTool);
        Invoker.getInstance().invoke(cmd);
    }

    private void btn2ActionPerformed() {
        CmdLoadFile cmd = new CmdLoadFile(this, editorTool);
        Invoker.getInstance().invoke(cmd);
    }

    private void btn3ActionPerformed() {
        CmdSaveFile cmd = new CmdSaveFile(this, editorTool);
        Invoker.getInstance().invoke(cmd);
    }

    private void btn4ActionPerformed() {
        bCheckSpell = !bCheckSpell;

        if(bCheckSpell) {
            checkSpell();
            btnMenu4.setBtnSkin(BtnSkinProvider.getInstance().getBtnSkin(EnumBtnSkin.BTN_CHECK));
        }else{
            resetCheckSpell();
            btnMenu4.setBtnSkin(BtnSkinProvider.getInstance().getBtnSkin(EnumBtnSkin.BTN_UNCHECK));
        }
    }

    private void formKeyReleased(KeyEvent evt) {
        if(KeyUtilities.getInstance().isModifierKey(evt))
            editorTool.setDirty(true);
        
        resetCheckSpell();
        checkSpell();
    }

    private void core() {
        statusText = "";

        if(btnMenu1.isFocused()) {
            statusText = "Hamorina asa vaovao";
        }else if(btnMenu2.isFocused()) {
            statusText = "Hanokatra asa efa misy";
        }else if(btnMenu3.isFocused()) {
            statusText = "Hitahiry asa";
        }else if(btnMenu4.isFocused()) {
            statusText = "Alefa/ajanona ny fitsarana diso tsipelina";
        }else if(btnNeura.isFocused()) {
            statusText = "Momba ny asa";
        }

        toolBar.repaint();
        statusBar.repaint();
    }

    public void run() {
        while(true) {
            try {
                core();
                Thread.sleep(30);
            } catch (InterruptedException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Since release2
    public void notify(String message) {
        try {
            DefaultStyledDocument doc = editorTool.getDocument();

            MyRobot.getInstance().sendKey(KeyEvent.VK_DELETE);
            int pos = editor.getSelectionStart();
            doc.insertString(pos, message, null);
        } catch (BadLocationException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
