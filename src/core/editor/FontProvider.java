package core.editor;

import java.awt.Font;

/**
 *
 * @author Mitanjo
 */
public class FontProvider {
    private static FontProvider instance = new FontProvider();

    private FontProvider() {
        initProvider();
    }

    public static FontProvider getInstance() {
        return instance;
    }

    private Font defaultFont;
    private Font statusbar_font;

    private void initProvider() {
        defaultFont = new Font("Arial", Font.PLAIN, 12);
        statusbar_font = new Font("Courier new", Font.PLAIN, 15);
    }    

    public Font getFont(EnumFont font) {
        switch(font) {
            case STATUSBAR_FONT: return statusbar_font;
        }

        return defaultFont;
    }
}
