package core.editor;

/**
 *
 * @author Mitanjo
 */
public enum EnumStyle {
    NORMAL_TEXT, INCORRECT_TEXT, UNDERLINED_TEXT, BOLD_TEXT
}
