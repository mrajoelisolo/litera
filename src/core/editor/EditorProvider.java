package core.editor;

import javax.swing.*;
import javax.swing.text.*;


/**
 *
 * @author Mitanjo
 */
public class EditorProvider {
    private static EditorProvider instance = new EditorProvider();

    private EditorProvider() {}

    public static EditorProvider getInstance() {
        return instance;
    }

    public EditorTool createEditorSet() {
        DefaultStyledDocument doc = new DefaultStyledDocument();
        JEditorPane editor = new JTextPane(doc);
        editor.setDragEnabled(true);
        editor.setBorder(BorderFactory.createEtchedBorder());
        return new EditorTool(editor, doc);
    }
}
