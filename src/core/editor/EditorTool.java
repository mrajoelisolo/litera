package core.editor;

import core.tools.FileFilterManager;
import core.tools.Invoker;
import core.tools.commands.CmdSaveFile;
import java.io.File;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.text.*;

/**
 *
 * @author Mitanjo
 */
public class EditorTool {
    private static JFileChooser fileChooser;
    private JEditorPane editorPane;
    private DefaultStyledDocument document;
    private boolean dirty = false;
    private String docPath = "";

    public EditorTool(JEditorPane editorPane, DefaultStyledDocument document) {
        if(fileChooser == null) {
            fileChooser = new JFileChooser();
            fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
            FileFilterManager.getInstance().setFileChooserFilter(fileChooser);
        }

        this.editorPane = editorPane;
        this.document = document;
    }

    public JEditorPane getEditorPane() {
        return editorPane;
    }

    public void setDocument(DefaultStyledDocument document) {
        this.editorPane.setDocument(document);
        this.document = document;
    }

    public DefaultStyledDocument getDocument() {
        return document;
    }

    public void resetDocument() {
        int n = document.getLength();
        document.setCharacterAttributes(0, n, StyleProvider.getInstance().getStyle(EnumStyle.NORMAL_TEXT), true);
    }

    public static void resetDocumentStyle(DefaultStyledDocument doc) {
        int n = doc.getLength();
        doc.setCharacterAttributes(0, n, StyleProvider.getInstance().getStyle(EnumStyle.NORMAL_TEXT), true);
    }

    public void clearDocument() {
        document = null;
        document = new DefaultStyledDocument();
        editorPane.setDocument(document);        

        setDocPath("");
        setDirty(false);
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }

    public void saveDocument(JFrame frame) {
        CmdSaveFile cmd = new CmdSaveFile(frame, this);
        Invoker.getInstance().invoke(cmd);
    }

    public boolean fileExists() {
        return new File(docPath).exists();
    }
}
