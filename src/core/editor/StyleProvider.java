package core.editor;

import java.awt.Color;
import java.util.Hashtable;
import javax.swing.text.*;

/**
 *
 * @author Mitanjo
 */
public class StyleProvider {
    private static StyleProvider instance = new StyleProvider();

    private StyleProvider() {
        initStyle();
    }

    public static StyleProvider getInstance() {
        return instance;
    }

    private Hashtable attributes = new Hashtable();
    private StyleContext styles = new StyleContext();

    private void initStyle() {
        Style s = styles.addStyle(null, null);
        attributes.put(EnumStyle.INCORRECT_TEXT, s);
        StyleConstants.setItalic(s, true);
        StyleConstants.setForeground(s, Color.RED);

        s = styles.addStyle(null, null);
        attributes.put(EnumStyle.NORMAL_TEXT, s);

        s = styles.addStyle(null, null);
        attributes.put(EnumStyle.UNDERLINED_TEXT, s);
        StyleConstants.setUnderline(s, true);

        s = styles.addStyle(null, null);
        attributes.put(EnumStyle.BOLD_TEXT, s);
        StyleConstants.setBold(s, true);
    }

    public Style getStyle(EnumStyle textStyle) {
        return (Style) attributes.get(textStyle);
    }
}
