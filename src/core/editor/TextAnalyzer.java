package core.editor;

import javax.swing.text.DefaultStyledDocument;
import core.tools.AutomatonSet;

/**
 *
 * @author Mitanjo
 */
public class TextAnalyzer {
    private static TextAnalyzer instance = new TextAnalyzer();

    private TextAnalyzer() {}

    public static TextAnalyzer getInstance() {
        return instance;
    }

    public void analyze(EditorTool editorTool) {
        if(editorTool == null) return;

        int count = 0;
        int k = 0;
        DefaultStyledDocument doc = editorTool.getDocument();
        String text = editorTool.getEditorPane().getText();

        String[] strs = text.replace("\n", " ").replace("\r", "").split(" "); //fix on 12/08/10
        for(String str : strs) {
            int start = count + k;
            int len = str.length();
            k++;

            String tstr = editorTool.getEditorPane().getText().substring(start, start + len);
            if(tstr.trim().length() == 0) continue;            

            String analyzedString = str.replace(",", "").replace(".", "").trim();

            boolean bCorrect = AutomatonSet.getInstance().analyze(analyzedString);
            if(!bCorrect) {
                doc.setCharacterAttributes(start, len, StyleProvider.getInstance().getStyle(EnumStyle.INCORRECT_TEXT), true);
            }

            count += str.length();
        }
    }
}
