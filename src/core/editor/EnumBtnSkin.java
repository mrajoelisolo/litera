package core.editor;

/**
 *
 * @author Mitanjo
 */
public enum EnumBtnSkin {
    BTN_NEW, BTN_OPEN, BTN_SAVE, BTN_CHECK, BTN_UNCHECK, BTN_EMPTY, BTN_NEURA;
}
