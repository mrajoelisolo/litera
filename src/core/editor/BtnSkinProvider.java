package core.editor;

import lwcanvas.components.LWButtonSkin;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class BtnSkinProvider {
    private static BtnSkinProvider instance = new BtnSkinProvider();

    private BtnSkinProvider() {
        initProvider();
    }

    private void initProvider() {
        String root = "img/toolbar/";
        
        btnEmptySkin = new LWButtonSkin(LWResourceLoader.getInstance().loadImage(root + "btn_0.png"), LWResourceLoader.getInstance().loadImage(root + "btn_1.png"), LWResourceLoader.getInstance().loadImage(root + "btn_2.png"));
        btnNewSkin = new LWButtonSkin(LWResourceLoader.getInstance().loadImage(root + "btn_new0.png"), LWResourceLoader.getInstance().loadImage(root + "btn_new1.png"), LWResourceLoader.getInstance().loadImage(root + "btn_new2.png"));
        btnOpenSkin = new LWButtonSkin(LWResourceLoader.getInstance().loadImage(root + "btn_open0.png"), LWResourceLoader.getInstance().loadImage(root + "btn_open1.png"), LWResourceLoader.getInstance().loadImage(root + "btn_open2.png"));
        btnSaveSkin = new LWButtonSkin(LWResourceLoader.getInstance().loadImage(root + "btn_save0.png"), LWResourceLoader.getInstance().loadImage(root + "btn_save1.png"), LWResourceLoader.getInstance().loadImage(root + "btn_save2.png"));
        btnCheckSkin = new LWButtonSkin(LWResourceLoader.getInstance().loadImage(root + "btn_check0.png"), LWResourceLoader.getInstance().loadImage(root + "btn_check1.png"), LWResourceLoader.getInstance().loadImage(root + "btn_check2.png"));
        btnUncheckSkin = new LWButtonSkin(LWResourceLoader.getInstance().loadImage(root + "btn_uncheck0.png"), LWResourceLoader.getInstance().loadImage(root + "btn_uncheck1.png"), LWResourceLoader.getInstance().loadImage(root + "btn_uncheck2.png"));
        btnNeura = new LWButtonSkin(LWResourceLoader.getInstance().loadImage(root + "btn_neura0.png"), LWResourceLoader.getInstance().loadImage(root + "btn_neura1.png"), LWResourceLoader.getInstance().loadImage(root + "btn_neura2.png"));
    }

    public static BtnSkinProvider getInstance() {
        return instance;
    }

    //private LWButtonSkin btnSaveSkin;
    private LWButtonSkin btnEmptySkin;
    private LWButtonSkin btnNewSkin;
    private LWButtonSkin btnOpenSkin;
    private LWButtonSkin btnSaveSkin;
    private LWButtonSkin btnCheckSkin;
    private LWButtonSkin btnUncheckSkin;
    private LWButtonSkin btnNeura;

    public LWButtonSkin getBtnSkin(EnumBtnSkin btnSkin) {
        switch(btnSkin) {
            case BTN_EMPTY: return btnEmptySkin;

            case BTN_NEW: return btnNewSkin;

            case BTN_OPEN: return btnOpenSkin;

            case BTN_SAVE: return btnSaveSkin;

            case BTN_CHECK: return btnCheckSkin;

            case BTN_UNCHECK: return btnUncheckSkin;

            case BTN_NEURA: return btnNeura;
        }

        return null;
    }
}
